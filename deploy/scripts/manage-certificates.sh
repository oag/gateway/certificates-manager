#!/bin/sh

if [ -z "${CERT_NAME}" ] || [ -z "${DOMAIN_LIST}" ] || [ -z "${EMAIL_LIST}" ]
then
	echo "CERT_NAME, DOMAIN_LIST and EMAIL_LIST variables should be defined!"
	exit 1
fi

fileToTestUpdate="/certs/live/${CERT_NAME}/chain.pem"
if [ -e "${fileToTestUpdate}" ]
then
	lastUpdateInSecondsBefore="$(stat -c %Y ${fileToTestUpdate})"
else
	lastUpdateInSecondsBefore=0
fi

if ! docker run --rm \
	-v ${CERTBOT_CONFIG_VOL_NAME}:/etc/letsencrypt \
	-v ${CERTBOT_WORK_VOL_NAME}:/var/lib/letsencrypt \
	-v ${CERTBOT_LOGS_VOL_NAME}:/var/log/letsencrypt \
	-v ${ACME_VOL_NAME}:/var/www/html \
	certbot/certbot certonly \
		--expand \
		--keep-until-expiring \
		--webroot -w /var/www/html/ \
		--cert-name ${CERT_NAME} \
		-m ${EMAIL_LIST} --agree-tos --no-eff-email \
		-d ${DOMAIN_LIST} \
		--no-self-upgrade
then
	echo "Certificates creation failed!"
	exit 1
fi

lastUpdateInSecondsAfter="$(stat -c %Y ${fileToTestUpdate})"

serverStack=$(echo "${SERVER_SERVICE}" | cut -f 1 -d '_')

if [ "${lastUpdateInSecondsBefore}" != "${lastUpdateInSecondsAfter}" ]
then
	echo "Certificates created for domains: ${DOMAIN_LIST}"
	echo "Updating certificates in web server service: ${SERVER_SERVICE}"

	secretFiles="chain fullchain privkey"
	secretRmParams=""
	secretAddParams=""

	for secretFile in ${secretFiles}
	do
		secretName="cert-${secretFile}"
		secretRmParams="${secretRmParams} --secret-rm ${secretName}"
		secretAddParams="${secretAddParams} --secret-add source=${secretName},target=/etc/nginx/certs/${secretFile}.pem"
	done

	docker service update ${secretRmParams} ${SERVER_SERVICE}

	for secretFile in ${secretFiles}
	do
		secretName="cert-${secretFile}"
		echo "Updating service secret: ${secretName}"

		docker secret rm ${secretName}

		cat /certs/live/${CERT_NAME}/${secretFile}.pem | docker secret create \
			-l com.docker.stack.namespace=${serverStack} \
			${secretName} -
	done

	docker service update ${secretAddParams} ${SERVER_SERVICE}

	echo "Certificates successfully updated!"
else
	echo "Certificates are still valid!"
fi
